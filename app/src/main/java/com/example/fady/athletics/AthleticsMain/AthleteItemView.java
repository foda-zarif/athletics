package com.example.fady.athletics.AthleticsMain;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.example.fady.athletics.AthleticsMain.AthleteModel.Athlete;
import com.example.fady.athletics.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AthleteItemView extends LinearLayout {
    @BindView(R.id.imgAthlete)
    ImageView ivAthlete;

    @BindView(R.id.tvAthleteName)
    TextView tvAthleteName;

    public AthleteItemView(Context context) {
        super(context);
        initView(context);
    }

    public AthleteItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public AthleteItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AthleteItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context) {

        LayoutInflater.from(context).inflate(R.layout.athlete_item_view, this);
        ButterKnife.bind(this);
    }

    public void setAthlete(Athlete athlete) {
        if (athlete.getImage() != null && !athlete.getImage().equals(""))
            Picasso.get().load(athlete.getImage()).placeholder(R.drawable.loading_image)
                    .into(ivAthlete);
        else {
            Picasso.get().load(R.drawable.not_available).placeholder(R.drawable.loading_image)
                    .into(ivAthlete);
        }
        tvAthleteName.setText(athlete.getName());
    }
}
