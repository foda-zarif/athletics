package com.example.fady.athletics.AthleticsDetails;

import com.example.fady.athletics.AthleticsMain.AthleteModel.Athlete;

public class AthleteDetailsPresenter implements IAthleteDetails.presenter {
    IAthleteDetails.View athleteView;

    public AthleteDetailsPresenter(IAthleteDetails.View athleteView) {
        this.athleteView = athleteView;
    }


    @Override
    public void checkAthleteData(Athlete athlete) {
        if (athlete.getImage() != null) {
            if (!athlete.getImage().isEmpty())
                athleteView.showAthleteImage();
        }
        athleteView.showAthleteDesc();
    }
}
