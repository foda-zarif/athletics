
package com.example.fady.athletics.AthleticsMain.AthleteModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AthleteResult {

    @SerializedName("athletes")
    private ArrayList<Athlete> mAthletes;

    public ArrayList<Athlete> getAthletes() {
        return mAthletes;
    }

    public void setAthletes(ArrayList<Athlete> athletes) {
        mAthletes = athletes;
    }

}
