
package com.example.fady.athletics.AthleticsMain.AthleteModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

// Athlete Model
public class Athlete implements Parcelable {

    @SerializedName("brief")
    private String mBrief;
    @SerializedName("image")
    private String mImage;
    @SerializedName("name")
    private String mName;

    protected Athlete(Parcel in) {
        mBrief = in.readString();
        mImage = in.readString();
        mName = in.readString();
    }

    public static final Creator<Athlete> CREATOR = new Creator<Athlete>() {
        @Override
        public Athlete createFromParcel(Parcel in) {
            return new Athlete(in);
        }

        @Override
        public Athlete[] newArray(int size) {
            return new Athlete[size];
        }
    };

    public String getBrief() {
        return mBrief;
    }

    public void setBrief(String brief) {
        mBrief = brief;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mBrief);
        parcel.writeString(mImage);
        parcel.writeString(mName);
    }
}
