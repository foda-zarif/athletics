package com.example.fady.athletics.WebService;

import com.example.fady.athletics.AthleticsMain.AthleteModel.AthleteResult;


import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    @GET("athletes.josn/")
    Call<AthleteResult> getAthletics();
}
