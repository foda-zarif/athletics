package com.example.fady.athletics.AthleticsDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.example.fady.athletics.AthleticsMain.AthleteModel.Athlete;
import com.example.fady.athletics.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.fady.athletics.Constant.ATHLETE_PARCELABLE_ID;

public class AthleteDetailsActivity extends AppCompatActivity implements IAthleteDetails.View {
    @BindView(R.id.imgAthlete)
    CircleImageView imgAthlete;

    @BindView(R.id.tvAthleteName)
    TextView tvAthleteName;

    @BindView(R.id.tvAthleteDesc)
    TextView tvAthleteDesc;

    Athlete athlete;

    IAthleteDetails.presenter athletePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_athlete_details);
        ButterKnife.bind(this);
        athlete = getIntent().getParcelableExtra(ATHLETE_PARCELABLE_ID);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        athletePresenter = new AthleteDetailsPresenter(this);
        athletePresenter.checkAthleteData(athlete);
    }


    @Override
    public void showAthleteImage() {
        imgAthlete.setVisibility(View.VISIBLE);
        Picasso.get().load(athlete.getImage()).placeholder(R.drawable.not_available).into(imgAthlete);

    }

    @Override
    public void showAthleteDesc() {
        tvAthleteName.setText(athlete.getName());
        tvAthleteDesc.setText(Html.fromHtml("<b>" + getString(R.string.athlete_brief_string_title) + " </b> " + athlete.getBrief()));
    }
}
