package com.example.fady.athletics.AthleticsMain;

import com.example.fady.athletics.AthleticsMain.AthleteModel.Athlete;

import java.util.ArrayList;

public interface IAthletics {

    interface AthleticsMainView {
        void showErrorMessage(String message);

        void showAthletes(ArrayList<Athlete> athletes);

        void showProgressDialog();

        void hideProgressDialog();
    }

    interface AthleticsMainPresenter {
        void getAthletes();
    }

}
