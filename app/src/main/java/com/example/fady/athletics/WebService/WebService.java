package com.example.fady.athletics.WebService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.fady.athletics.Constant.BASE_WEB_SERVICE_URL;

public class WebService {
    private static WebService webService;
    private Api api;

    Gson gson = new GsonBuilder().setLenient().create();

    private WebService() {
        OkHttpClient client = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_WEB_SERVICE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
        api = retrofit.create(Api.class);

    }

    public static WebService getWebServiceInstance() {
        if (webService == null)
            webService = new WebService();

        return webService;
    }

    public Api getApi() {
        return api;
    }
}
