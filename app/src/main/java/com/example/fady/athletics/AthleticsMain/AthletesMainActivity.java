package com.example.fady.athletics.AthleticsMain;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.fady.athletics.AthleticsDetails.AthleteDetailsActivity;
import com.example.fady.athletics.AthleticsMain.AthleteModel.Athlete;
import com.example.fady.athletics.R;
import com.example.fady.athletics.WebService.NetworkCheck;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.fady.athletics.Constant.ATHLETE_PARCELABLE_ID;

public class AthletesMainActivity extends AppCompatActivity implements AthleteOnClickInterface, IAthletics.AthleticsMainView {
    //    private RecyclerView rvAthletics;
    private AthletesAdapter athletesAdapter;
    private ArrayList<Athlete> athleteArrayList;
    private IAthletics.AthleticsMainPresenter athleticsMainPresenter;
    private ProgressDialog progressDialog;

    @BindView(R.id.rvAthletics)
    RecyclerView rvAthletics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        getAthletesList();
    }

    private void initView() {
        ButterKnife.bind(this);
        athleticsMainPresenter = new AthletesMainPresenter(this);
        rvAthletics.setLayoutManager(new GridLayoutManager(this, 2));
        athleteArrayList = new ArrayList<>();


    }

    private void getAthletesList() {
        if (NetworkCheck.isAvailable(this))
            athleticsMainPresenter.getAthletes();
        else showErrorMessage(getString(R.string.internet_error_message));
    }

    @Override
    public void athleteOnClickListner(int pos) {
        // TODO: 2019-08-05  send clicked athlete information to detailed activity
        Intent intent = new Intent(this, AthleteDetailsActivity.class);
        intent.putExtra(ATHLETE_PARCELABLE_ID, athleteArrayList.get(pos));
        startActivity(intent);
    }

    @Override
    public void showErrorMessage(String message) {
        if (progressDialog != null)
            progressDialog.dismiss();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAthletes(ArrayList<Athlete> athletes) {
        athleteArrayList = athletes;
        athletesAdapter = new AthletesAdapter(athleteArrayList, this);
        rvAthletics.setAdapter(athletesAdapter);

    }

    @Override
    public void showProgressDialog() {
        progressDialog = ProgressDialog.show(this, getString(R.string.progress_loading_message), null, true, false);
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }
}
