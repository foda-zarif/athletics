package com.example.fady.athletics.AthleticsMain;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fady.athletics.AthleticsMain.AthleteModel.Athlete;

import java.util.ArrayList;

public class AthletesAdapter extends RecyclerView.Adapter<AthletesAdapter.AthleticsViewHolder> {
    ArrayList<Athlete> athletesList;
    AthleteOnClickInterface athleteOnClickInterface;

    public AthletesAdapter(ArrayList<Athlete> athletes, AthleteOnClickInterface athleteOnClickInterface) {
        this.athletesList = athletes;
        this.athleteOnClickInterface = athleteOnClickInterface;
    }

    @NonNull
    @Override
    public AthleticsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AthleteItemView athleteItemView = new AthleteItemView(parent.getContext());
        return new AthleticsViewHolder(athleteItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AthleticsViewHolder holder, final int position) {
        // setting the athlete info
        holder.athleteItemView.setAthlete(athletesList.get(position));
        holder.athleteItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                athleteOnClickInterface.athleteOnClickListner(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return athletesList.size();
    }

    public class AthleticsViewHolder extends RecyclerView.ViewHolder {
        // Athlete Card that display athlete name, athlete pic
        AthleteItemView athleteItemView;

        public AthleticsViewHolder(@NonNull AthleteItemView itemView) {
            super(itemView);
            this.athleteItemView = itemView;
        }
    }
}
