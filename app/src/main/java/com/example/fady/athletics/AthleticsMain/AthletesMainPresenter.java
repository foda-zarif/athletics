package com.example.fady.athletics.AthleticsMain;

import com.example.fady.athletics.AthleticsMain.AthleteModel.AthleteResult;
import com.example.fady.athletics.WebService.NetworkCheck;
import com.example.fady.athletics.WebService.WebService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AthletesMainPresenter implements IAthletics.AthleticsMainPresenter {
    IAthletics.AthleticsMainView athleticsMainView;

    public AthletesMainPresenter(IAthletics.AthleticsMainView athleticsMainView) {
        this.athleticsMainView = athleticsMainView;
    }

    @Override
    public void getAthletes() {
        athleticsMainView.showProgressDialog();
            Call<AthleteResult> athleteResultCall = WebService.getWebServiceInstance().getApi().getAthletics();
        athleteResultCall.enqueue(new Callback<AthleteResult>() {
            @Override
            public void onResponse(Call<AthleteResult> call, Response<AthleteResult> response) {
                athleticsMainView.showAthletes(response.body().getAthletes());
                athleticsMainView.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<AthleteResult> call, Throwable t) {
                athleticsMainView.showErrorMessage(t.getLocalizedMessage());
            }
        });
    }
}
