package com.example.fady.athletics.AthleticsDetails;

import com.example.fady.athletics.AthleticsMain.AthleteModel.Athlete;

public interface IAthleteDetails {
    interface View {
        void showAthleteImage();

        void showAthleteDesc();
    }

    interface presenter {
        void checkAthleteData(Athlete athlete);
    }
}
